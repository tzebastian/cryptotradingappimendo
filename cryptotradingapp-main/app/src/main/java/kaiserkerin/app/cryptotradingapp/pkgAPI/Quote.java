package kaiserkerin.app.cryptotradingapp.pkgAPI;
import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Quote implements Serializable
{

    @SerializedName("EUR")
    @Expose
    private EUR eUR;
    private final static long serialVersionUID = -5780538494495942860L;

    public EUR getEUR() {
        return eUR;
    }

    public void setEUR(EUR eUR) {
        this.eUR = eUR;
    }

}