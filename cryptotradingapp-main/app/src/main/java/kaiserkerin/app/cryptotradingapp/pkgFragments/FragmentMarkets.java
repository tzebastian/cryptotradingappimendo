package kaiserkerin.app.cryptotradingapp.pkgFragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.util.ArrayList;

import kaiserkerin.app.cryptotradingapp.R;
import kaiserkerin.app.cryptotradingapp.pkgAPI.APIClient;
import kaiserkerin.app.cryptotradingapp.pkgAPI.APIInterface;
import kaiserkerin.app.cryptotradingapp.pkgAPI.CryptoCurrDataPoint;
import kaiserkerin.app.cryptotradingapp.pkgAPI.CryptoList;
import kaiserkerin.app.cryptotradingapp.pkgMisc.CryptoListAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentMarkets extends Fragment implements AdapterView.OnItemClickListener {
    View view;
    ListView listCryptoDates;
    RelativeLayout loadingPanel;

    APIInterface apiClient;
    ArrayList<CryptoCurrDataPoint> cryptoList = null;
    CryptoListAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_market, container, false);

        getAllViews();
        initOtherThings();
        registrateEventhandlers();

        return view;
    }

    private void getAllViews() {
        listCryptoDates = view.findViewById(R.id.listCryptoDates);
        loadingPanel = view.findViewById(R.id.loadingPanel);
    }

    private void initOtherThings() {
        apiClient = APIClient.getClient().create(APIInterface.class);
        cryptoList = new ArrayList<>();
        adapter = new CryptoListAdapter(view.getContext(), cryptoList);
        listCryptoDates.setAdapter(adapter);
        getCoinList();
    }

    private void registrateEventhandlers() {
        listCryptoDates.setOnItemClickListener(this);
    }

    private void getCoinList() {
        Call<CryptoList> call2 = apiClient.doGetUserList("100");
        call2.enqueue(new Callback<CryptoList>() {
            @Override
            public void onResponse(Call<CryptoList> call, Response<CryptoList> response) {
                CryptoList list = response.body();
                CryptoList.setInstance(list);

                cryptoList.clear();
                cryptoList.addAll(list.getData());

                CryptoListAdapter adapter = new CryptoListAdapter(view.getContext(), cryptoList);
                listCryptoDates.setAdapter(adapter);
                loadingPanel.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onFailure(Call<CryptoList> call, Throwable t) {
                Toast.makeText(getContext(), "onFailure", Toast.LENGTH_SHORT).show();
                Log.d("XXXX", t.getLocalizedMessage());
                call.cancel();
            }
        });
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        CryptoCurrDataPoint selectedCrypto = (CryptoCurrDataPoint) adapterView.getItemAtPosition(i);
        FragmentCryptoDetails fragment = new FragmentCryptoDetails();
        Bundle args = new Bundle();
        args.putSerializable("selectedCrypto", selectedCrypto);
        fragment.setArguments(args);
        getParentFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment).addToBackStack(null).commit();
    }
}
