package kaiserkerin.app.cryptotradingapp.pkgMisc;

import android.content.Context;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;

import kaiserkerin.app.cryptotradingapp.R;
import kaiserkerin.app.cryptotradingapp.pkgData.Trade;

public class CustomizedAdapterTrade extends BaseAdapter {

    ArrayList<Trade> collTrades = new ArrayList<>();
    LayoutInflater inflater;

    public CustomizedAdapterTrade(Context applicationContext, Collection<Trade> collTrades) {
        this.collTrades.addAll(collTrades);
        this.inflater = LayoutInflater.from(applicationContext);
    }

    @Override
    public int getCount() {
        return collTrades.size();
    }

    @Override
    public Object getItem(int i) {
        return collTrades.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        DecimalFormat df = new DecimalFormat();
        df.setMaximumFractionDigits(6);

        view = inflater.inflate(R.layout.transaction_listentry, null);
        Trade trade = (Trade) getItem(i);

        TextView transactionSymbol = view.findViewById(R.id.txtTransactionSymbol);
        TextView transactionAmount = view.findViewById(R.id.txtTransactionAmount);
        TextView transactionDate = view.findViewById(R.id.txtTransactionDate);
        TextView transactionPrice = view.findViewById(R.id.txtTransactionPrice);

        transactionSymbol.setText(trade.getRootCurrSymbol() + " ➔ " + trade.getSymbol());
        transactionAmount.setText(df.format(trade.getAmount()) + " " + trade.getSymbol());
        transactionDate.setText(trade.getDate().toString());
        transactionPrice.setText(df.format(trade.getAmountInRootCurr()) + " " + trade.getRootCurrSymbol());

        return view;
    }
}
