package kaiserkerin.app.cryptotradingapp.pkgAPI;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

public interface APIInterface {
    @Headers("X-CMC_PRO_API_KEY:ecb1820b-fc09-410c-977e-001531bb7bbf")
    @GET("/v1/cryptocurrency/listings/latest?convert=EUR")
    Call<CryptoList> doGetUserList(@Query("limit") String page);
}
