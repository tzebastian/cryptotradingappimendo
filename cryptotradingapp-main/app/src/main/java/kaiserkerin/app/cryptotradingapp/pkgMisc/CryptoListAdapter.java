package kaiserkerin.app.cryptotradingapp.pkgMisc;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import kaiserkerin.app.cryptotradingapp.R;
import kaiserkerin.app.cryptotradingapp.pkgAPI.CryptoCurrDataPoint;

public class CryptoListAdapter extends BaseAdapter {

    ArrayList<CryptoCurrDataPoint> collCryptoCurrDataPoints;
    LayoutInflater inflater;

    public CryptoListAdapter(Context applicationContext, ArrayList list) {
        this.collCryptoCurrDataPoints = list;
        this.inflater = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return collCryptoCurrDataPoints.size();
    }

    @Override
    public Object getItem(int i) {
        return collCryptoCurrDataPoints.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflater.inflate(R.layout.crypto_list_item, null);
        CryptoCurrDataPoint cryptoCurrDataPoint = collCryptoCurrDataPoints.get(i);

        TextView name = view.findViewById(R.id.name);
        name.setText(cryptoCurrDataPoint.getName() + " (" + cryptoCurrDataPoint.getSymbol() + ")");

        TextView price = view.findViewById(R.id.price);
        price.setText("Price: €" + String.format("%,f", cryptoCurrDataPoint.getQuote().getEUR().getPrice()));

        TextView marketCap = view.findViewById(R.id.marketCap);
        marketCap.setText("Market Cap: €" + String.format("%,d", Math.round(cryptoCurrDataPoint.getQuote().getEUR().getMarketCap())));

        TextView volume24h = view.findViewById(R.id.volume24h);
        volume24h.setText("Volume/24h: €" + String.format("%,d", Math.round(cryptoCurrDataPoint.getQuote().getEUR().getVolume24h())));

        TextView textView1h = view.findViewById(R.id.textView1h);
        Double percentageChange1h = cryptoCurrDataPoint.getQuote().getEUR().getPercentChange1h();
        textView1h.setText(String.format("1h:" + ((percentageChange1h < 0) ? "" : " ") + " %.2f", percentageChange1h) + "%");
        if (percentageChange1h < 0)
            textView1h.setTextColor(Color.RED);
        else
            textView1h.setTextColor(view.getResources().getColor(R.color.darkgreen, null));

        TextView textView24h = view.findViewById(R.id.textView24h);
        Double percentageChange24h = cryptoCurrDataPoint.getQuote().getEUR().getPercentChange24h();
        textView24h.setText(String.format("24h:" + ((percentageChange24h < 0) ? "" : " ") + " %.2f", cryptoCurrDataPoint.getQuote().getEUR().getPercentChange24h()) + "%");
        if (percentageChange24h < 0)
            textView24h.setTextColor(Color.RED);
        else
            textView24h.setTextColor(view.getResources().getColor(R.color.darkgreen, null));

        TextView textView7d = view.findViewById(R.id.textView7d);
        Double percentageChange7d = cryptoCurrDataPoint.getQuote().getEUR().getPercentChange7d();
        textView7d.setText(String.format("7d:" + ((percentageChange7d < 0) ? "" : " ") + " %.2f", cryptoCurrDataPoint.getQuote().getEUR().getPercentChange7d()) + "%");
        if (percentageChange7d < 0)
            textView7d.setTextColor(Color.RED);
        else
            textView7d.setTextColor(view.getResources().getColor(R.color.darkgreen, null));

        return view;
    }

    @Override
    public void notifyDataSetChanged() {

    }
}