package kaiserkerin.app.cryptotradingapp.pkgMisc;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import kaiserkerin.app.cryptotradingapp.R;
import kaiserkerin.app.cryptotradingapp.pkgAPI.CryptoCurrDataPoint;
import kaiserkerin.app.cryptotradingapp.pkgData.User;
import kaiserkerin.app.cryptotradingapp.pkgFirestore.OnReadWriteCompleted;

public class BuyDialog extends BottomSheetDialog implements android.view.View.OnClickListener, AdapterView.OnItemSelectedListener, OnReadWriteCompleted {
    public static CryptoCurrDataPoint selectedCrypto;

    public Activity c;
    public Dialog d;

    TextView lblCryptoSymbol;
    Spinner spinnerRootCurrency;
    EditText txtAmount;
    Button bttnBuy;

    public BuyDialog(Activity a) {
        super(a);
        this.c = a;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.buy_dialog);

        getViews();
        initOtherThings();
        registerEventhandler();
    }

    private void getViews() {
        lblCryptoSymbol = findViewById(R.id.lblCryptoSymbol);
        spinnerRootCurrency = findViewById(R.id.spinnerRootCurrency);
        txtAmount = findViewById(R.id.txtAmount);
        bttnBuy = findViewById(R.id.bttnBuy);
    }

    private void initOtherThings() {
        Set<String> availableCurrencySymbols = User.getInstance().getPortfolio().keySet();
        availableCurrencySymbols.remove(selectedCrypto.getSymbol());
        List<String> availableCurrencies = new ArrayList<>(availableCurrencySymbols);

        System.out.println(User.getInstance().getUsername() + " " + availableCurrencies);
        lblCryptoSymbol.setText("BUY " + selectedCrypto.getSymbol());
        spinnerRootCurrency.setAdapter(new ArrayAdapter<String>(
                getContext(),
                R.layout.support_simple_spinner_dropdown_item,
                availableCurrencies));
    }

    private void registerEventhandler() {
        spinnerRootCurrency.setOnItemSelectedListener(this);
        bttnBuy.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        try {
            switch (v.getId()) {
                case R.id.bttnBuy:
                    String amount = txtAmount.getText().toString();

                    if (amount.length() == 0)
                        throw new Exception("Amount cannot be empty!");
                    else {
                        System.out.println("request to buy " + selectedCrypto.getSymbol() + " of amount: " + txtAmount.getText() + " " + spinnerRootCurrency.getSelectedItem());
                        User.getInstance().buyCrypto(
                                selectedCrypto.getSymbol(),
                                spinnerRootCurrency.getSelectedItem().toString(),
                                Double.parseDouble(amount));
                        User.saveToDB(this);
                    }
                    break;
                default:
                    break;
            }
            dismiss();
        } catch (Exception ex) {
            ToastManager.createErrorToast(getContext(), ex.getMessage());
            ex.printStackTrace();
        }
    }


    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        txtAmount.setHint("amount to spend in " + spinnerRootCurrency.getSelectedItem().toString());
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
        spinnerRootCurrency.setSelection(0);
    }

    @Override
    public void onReadCompleted(Collection<?> collData) {

    }

    @Override
    public void onWriteCompleted(String message) {
        ToastManager.createSuccessToast(getContext(), "completed!");
    }

    @Override
    public void onChangeDoc(String message) {

    }

    @Override
    public void onError(String message) {

    }
}