package kaiserkerin.app.cryptotradingapp.pkgFirestore;

import android.util.Log;

import androidx.annotation.Nullable;

import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

import kaiserkerin.app.cryptotradingapp.pkgData.User;

public class FirestoreReadUser implements EventListener<QuerySnapshot> {
    private static String COLL_USERS = "collUsers";
    private FirebaseFirestore ffdb;
    private ArrayList<User> collUsers = new ArrayList<>();
    private OnReadWriteCompleted listener;

    public FirestoreReadUser(OnReadWriteCompleted listener){
        this.listener = listener;
        ffdb = FirebaseFirestore.getInstance();
    }

    public void doReading(String usernameToSearch){
        collUsers.clear();
        ffdb.collection(COLL_USERS)
                .orderBy("username")
                .startAt(usernameToSearch)
                .endAt(usernameToSearch)
                .addSnapshotListener(this);
    }

    @Override
    public void onEvent(@Nullable QuerySnapshot value, @Nullable FirebaseFirestoreException error){
        if(value != null){
            for(DocumentSnapshot document : value.getDocuments()){
                collUsers.add(document.toObject(User.class));
                document.getReference().addSnapshotListener(new EventListener<DocumentSnapshot>() {
                    @Override
                    public void onEvent(@Nullable DocumentSnapshot docsnapshot, @Nullable FirebaseFirestoreException e) {
                        if(e != null){
                            System.out.println("***** error onChange-Event: Listen failed with " + e.getMessage());
                        } else if(docsnapshot != null && docsnapshot.exists()){
                            System.out.println("***** onChange-Event doc changed: " + docsnapshot.getData());
                            listener.onChangeDoc("doc change in background: " + docsnapshot.getData());
                        } else {
                            System.out.println("***** onChange-Event deleted: deleted doc: " + docsnapshot.getData());
                            listener.onChangeDoc("doc deleted in background");
                        }
                    }
                });
            }
            Log.i("** onCompleted **", "...read users completed");
            listener.onReadCompleted(collUsers);
        }
    }
}
