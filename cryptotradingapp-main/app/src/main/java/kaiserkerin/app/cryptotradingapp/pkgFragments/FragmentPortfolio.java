package kaiserkerin.app.cryptotradingapp.pkgFragments;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import kaiserkerin.app.cryptotradingapp.R;
import kaiserkerin.app.cryptotradingapp.pkgAPI.APIClient;
import kaiserkerin.app.cryptotradingapp.pkgAPI.APIInterface;
import kaiserkerin.app.cryptotradingapp.pkgAPI.CryptoList;
import kaiserkerin.app.cryptotradingapp.pkgData.User;
import kaiserkerin.app.cryptotradingapp.pkgMisc.CryptoListAdapter;
import kaiserkerin.app.cryptotradingapp.pkgMisc.CustomValueFormatter;
import kaiserkerin.app.cryptotradingapp.pkgMisc.CustomizedAdapterPortfolio;
import kaiserkerin.app.cryptotradingapp.pkgMisc.ToastManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentPortfolio extends Fragment {

    View view;
    User currentUser;

    ListView listPortfolio;
    PieChart pcPortfolio;
    RelativeLayout loadingPanel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_portfolio, container, false);

        getAllViews();
        checkForCryptolistAndInit();

        return view;
    }

    private void getAllViews() {
        listPortfolio = view.findViewById(R.id.listPortfolio);
        pcPortfolio = view.findViewById(R.id.pcPortfolio);
        loadingPanel = view.findViewById(R.id.loadingPanel);
    }

    private void initOtherThings() {
        currentUser = User.getInstance();
        fillPieChartPortfolio(currentUser.getPortfolio());
        initPieChart();
        fillListViewPortfolio(currentUser.getPortfolio());
        loadingPanel.setVisibility(View.INVISIBLE);
    }

    private void checkForCryptolistAndInit() {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                try {
                    Log.i("***info", "trying to load and init");
                    getCoinList();
                    CryptoList.getCryptoList();
                    initOtherThings();
                } catch (Exception e) {
                    Log.i("****info", e.getMessage());
                    e.printStackTrace();

                    setTimeout(() -> {
                        checkForCryptolistAndInit();
                    }, 1000);
                }
            }
        });
    }


    private void getCoinList() {
        APIInterface apiClient = APIClient.getClient().create(APIInterface.class);
        Call<CryptoList> call2 = apiClient.doGetUserList("100");
        call2.enqueue(new Callback<CryptoList>() {
            @Override
            public void onResponse(Call<CryptoList> call, Response<CryptoList> response) {
                CryptoList list = response.body();
                CryptoList.setInstance(list);
            }

            @Override
            public void onFailure(Call<CryptoList> call, Throwable t) {
                Log.d("XXXX", t.getLocalizedMessage());
                call.cancel();
            }
        });
    }

    private void initPieChart() {

        //remove the description label on the lower left corner, default true if not set
        pcPortfolio.getDescription().setEnabled(false);

        pcPortfolio.getDescription().setEnabled(false);

        //enabling the user to rotate the chart, default true
        pcPortfolio.setRotationEnabled(true);
        //adding friction when rotating the pie chart
        pcPortfolio.setDragDecelerationFrictionCoef(0.9f);
        //setting the first entry start from right hand side, default starting from top
        pcPortfolio.setRotationAngle(0);

        //highlight the entry when it is tapped, default true if not set
        pcPortfolio.setHighlightPerTapEnabled(true);
        //adding animation so the entries pop up from 0 degree
        pcPortfolio.animateY(1400, Easing.EasingOption.EaseInOutQuad);
        //setting the color of the hole in the middle, default white
        pcPortfolio.setHoleColor(getResources().getColor(R.color.background));
        pcPortfolio.setHoleRadius(0f);
        pcPortfolio.setTransparentCircleRadius(0f);
        pcPortfolio.getLegend().setTextColor(getResources().getColor(R.color.white));

    }

    private void fillPieChartPortfolio(HashMap<String, Double> portfolio) {

        ArrayList<PieEntry> pieEntries = new ArrayList<>();
        String label = "";

        //initializing data
        Map<String, Double> typeAmountMap = new HashMap<>();

        //initializing colors for the entries
        ArrayList<Integer> colors = new ArrayList<>();
        portfolio.forEach((String key, Double amount) -> {
            typeAmountMap.put(key, CryptoList.getCurrentFiatValue(key, amount));
            colors.add(generateRandomColor(key));
        });

        //input data and fit data into pie chart entry
        for (String type : typeAmountMap.keySet()) {
            pieEntries.add(new PieEntry(typeAmountMap.get(type).floatValue(), type));
        }

        //collecting the entries with label name
        PieDataSet pieDataSet = new PieDataSet(pieEntries, label);
        //setting text size of the value
        pieDataSet.setValueTextSize(12f);
        //providing color list for coloring different entries
        pieDataSet.setColors(colors);
        //grouping the data set from entry to chart
        PieData pieData = new PieData(pieDataSet);
        //showing the value of the entries, default true if not set
        pieData.setDrawValues(true);
        pieData.setValueFormatter(new CustomValueFormatter());

        pcPortfolio.setData(pieData);
        pcPortfolio.invalidate();
    }

    private void fillListViewPortfolio(Map<String, Double> collPortfolio) {
        CustomizedAdapterPortfolio adapter = new CustomizedAdapterPortfolio(getContext(), collPortfolio);
        listPortfolio.setAdapter(adapter);
    }

    public int generateRandomColor(String seed) {
        Random mRandom = new Random(stringToSeed(seed));
        final int baseColor = Color.WHITE;

        final int baseRed = Color.red(baseColor);
        final int baseGreen = Color.green(baseColor);
        final int baseBlue = Color.blue(baseColor);

        final int red = (baseRed + mRandom.nextInt(256)) / 2;
        final int green = (baseGreen + mRandom.nextInt(256)) / 2;
        final int blue = (baseBlue + mRandom.nextInt(256)) / 2;

        return Color.rgb(red, green, blue);
    }

    public long stringToSeed(String s) {
        if (s == null) {
            return 0;
        }
        long hash = 0;
        for (char c : s.toCharArray()) {
            hash = 31L * hash + c;
        }
        return hash;
    }

    public void setTimeout(Runnable runnable, int delay) {
        new Thread(() -> {
            try {
                Thread.sleep(delay);
                runnable.run();
            } catch (Exception e) {
                System.err.println(e);
            }
        }).start();
    }
}
