package kaiserkerin.app.cryptotradingapp.pkgFragments;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import kaiserkerin.app.cryptotradingapp.R;
import kaiserkerin.app.cryptotradingapp.pkgAPI.CryptoCurrDataPoint;
import kaiserkerin.app.cryptotradingapp.pkgData.Date;
import kaiserkerin.app.cryptotradingapp.pkgMisc.BuyDialog;
import kaiserkerin.app.cryptotradingapp.pkgMisc.SellDialog;
import kaiserkerin.app.cryptotradingapp.pkgMisc.ToastManager;

public class FragmentCryptoDetails extends Fragment implements View.OnClickListener {

    View view;

    TextView lblName;
    TextView lblPrice;
    TextView lblLastUpdated;
    TextView lblSymbol;
    TextView lblVolume24h;
    TextView lblCirculatingSupply;
    TextView lblMaxSupply;
    TextView lblMarketCap;
    TextView lblChange1h;
    TextView lblChange24h;
    TextView lblChange7d;

    Button bttnBuy;
    Button bttnSell;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_cryptodetails, container, false);

        getAllViews();
        registerEventhandlers();
        initOtherThings();
        fillLabels();

        return view;
    }

    CryptoCurrDataPoint selectedCrypto;

    private void getAllViews() {
        lblName = view.findViewById(R.id.lblName);
        lblPrice = view.findViewById(R.id.lblPrice);
        lblLastUpdated = view.findViewById(R.id.lblLastUpdated);
        lblSymbol = view.findViewById(R.id.lblSymbol);
        lblVolume24h = view.findViewById(R.id.lblVolume24h);
        lblCirculatingSupply = view.findViewById(R.id.lblCirculatingSupply);
        lblMaxSupply = view.findViewById(R.id.lblMaxSupply);
        lblMarketCap = view.findViewById(R.id.lblMarketCap);
        lblChange1h = view.findViewById(R.id.lblChange1h);
        lblChange24h = view.findViewById(R.id.lblChange24h);
        lblChange7d = view.findViewById(R.id.lblChange7d);

        bttnBuy = view.findViewById(R.id.bttnBuy);
        bttnSell = view.findViewById(R.id.bttnSell);
    }

    private void registerEventhandlers() {
        bttnBuy.setOnClickListener(this);
        bttnSell.setOnClickListener(this);
    }

    private void initOtherThings() {
        selectedCrypto = (CryptoCurrDataPoint) getArguments().get("selectedCrypto");
    }

    private void fillLabels() {
        try {
            lblName.setText(selectedCrypto.getName() + " (" + selectedCrypto.getSymbol() + ")");
            lblPrice.setText("Price: €" + String.format("%,f", selectedCrypto.getQuote().getEUR().getPrice()));
            lblLastUpdated.setText("last updated: " + new Date(selectedCrypto.getLastUpdated()).toString());

            lblSymbol.setText("Symbol: " + selectedCrypto.getSymbol());
            lblVolume24h.setText("Volume/24h: " + String.format("%,d", Math.round(selectedCrypto.getQuote().getEUR().getVolume24h())));

            lblCirculatingSupply.setText("Circulating supply: " + String.format("%.0f", selectedCrypto.getCirculatingSupply()) + " " + selectedCrypto.getSymbol());
            lblMaxSupply.setText("Max supply: " + String.format("%.0f", selectedCrypto.getMaxSupply()) + " " + selectedCrypto.getSymbol());

            lblMarketCap.setText("Market Cap: €" + String.format("%,d", Math.round(selectedCrypto.getQuote().getEUR().getMarketCap())));
            Double percentageChange1h = selectedCrypto.getQuote().getEUR().getPercentChange1h();
            lblChange1h.setText(String.format("1h:" + ((percentageChange1h < 0) ? "" : " ") + " %.2f", percentageChange1h) + "%");
            if (percentageChange1h < 0)
                lblChange1h.setTextColor(Color.RED);
            else
                lblChange1h.setTextColor(view.getResources().getColor(R.color.darkgreen, null));

            Double percentageChange24h = selectedCrypto.getQuote().getEUR().getPercentChange24h();
            lblChange24h.setText(String.format("24h:" + ((percentageChange24h < 0) ? "" : " ") + " %.2f", selectedCrypto.getQuote().getEUR().getPercentChange24h()) + "%");
            if (percentageChange24h < 0)
                lblChange24h.setTextColor(Color.RED);
            else
                lblChange24h.setTextColor(view.getResources().getColor(R.color.darkgreen, null));

            Double percentageChange7d = selectedCrypto.getQuote().getEUR().getPercentChange7d();
            lblChange7d.setText(String.format("7d:" + ((percentageChange7d < 0) ? "" : " ") + " %.2f", selectedCrypto.getQuote().getEUR().getPercentChange7d()) + "%");
            if (percentageChange7d < 0)
                lblChange7d.setTextColor(Color.RED);
            else
                lblChange7d.setTextColor(view.getResources().getColor(R.color.darkgreen, null));
        } catch (Exception ex) {
            ToastManager.createErrorToast(getContext(), ex.getMessage());
            ex.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.bttnBuy:
                BuyDialog.selectedCrypto = selectedCrypto;
                BuyDialog alertD = new BuyDialog(getActivity());
                alertD.show();
                break;
            case R.id.bttnSell:
                SellDialog.selectedCrypto = selectedCrypto;
                SellDialog sellDialog = new SellDialog(getActivity());
                sellDialog.show();
                break;
        }
    }
}
