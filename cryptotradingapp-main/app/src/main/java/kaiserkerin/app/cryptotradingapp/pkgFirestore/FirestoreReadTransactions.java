package kaiserkerin.app.cryptotradingapp.pkgFirestore;

import android.util.Log;

import androidx.annotation.Nullable;

import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FieldPath;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

import kaiserkerin.app.cryptotradingapp.pkgData.Trade;

public class FirestoreReadTransactions implements EventListener<QuerySnapshot> {

    private static String COLL_USER = "collUsers";
    private static String COLL_TRANSACTIONS = "collTransactions";
    private FirebaseFirestore ffdb;
    private OnReadWriteCompleted listener;

    public FirestoreReadTransactions(OnReadWriteCompleted listener) {
        this.listener = listener;
        this.ffdb = FirebaseFirestore.getInstance();
    }

    public void doReading(String username) {
        ffdb.collection(COLL_USER)
                .document(username)
                .collection(COLL_TRANSACTIONS)
                .orderBy(FieldPath.documentId(), Query.Direction.DESCENDING)
                .addSnapshotListener(this);
    }

    @Override
    public void onEvent(@Nullable QuerySnapshot value, @Nullable FirebaseFirestoreException error) {
        if (error == null) {
            ArrayList<Trade> collTransactions = new ArrayList<Trade>();
            for (DocumentSnapshot doc : value.getDocuments()) {
                collTransactions.add(doc.toObject(Trade.class));
            }
            Log.i("** onCompleted **", "...read transactions completed");
            listener.onReadCompleted(collTransactions);
        } else {
            Log.e("ERROR", error.getMessage());
        }
    }
}
