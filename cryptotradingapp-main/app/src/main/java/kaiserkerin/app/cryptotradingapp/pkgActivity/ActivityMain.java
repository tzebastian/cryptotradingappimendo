package kaiserkerin.app.cryptotradingapp.pkgActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationBarView;
import com.google.android.material.navigation.NavigationView;

import kaiserkerin.app.cryptotradingapp.R;
import kaiserkerin.app.cryptotradingapp.pkgAPI.APIClient;
import kaiserkerin.app.cryptotradingapp.pkgAPI.APIInterface;
import kaiserkerin.app.cryptotradingapp.pkgFragments.FragmentMarkets;
import kaiserkerin.app.cryptotradingapp.pkgFragments.FragmentPortfolio;
import kaiserkerin.app.cryptotradingapp.pkgFragments.FragmentProfile;
import kaiserkerin.app.cryptotradingapp.pkgFragments.FragmentTransactions;

public class ActivityMain extends AppCompatActivity implements NavigationBarView.OnItemSelectedListener {

    private BottomNavigationView navBottom;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_container);
        try {
            getAllViews();
            registerEventhandlers();
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new FragmentMarkets()).commit();
        } catch (Exception ex) {
            Log.e("******* ERROR *******", ex.getMessage());
        }
    }

    private void getAllViews() {
        navBottom = findViewById(R.id.menu_navigation);
    }

    private void registerEventhandlers() {
        navBottom.setOnItemSelectedListener(this);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Fragment selectedFragment = null;

        switch (item.getItemId()) {
            case R.id.mItemMarket:
                selectedFragment = new FragmentMarkets();
                break;
            case R.id.mItemPortfolio:
                selectedFragment = new FragmentPortfolio();
                break;
            case R.id.mItemTransactions:
                selectedFragment = new FragmentTransactions();
                break;
            case R.id.mItemProfile:
                selectedFragment = new FragmentProfile();
                break;
        }
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, selectedFragment).commit();
        return true;
    }
}
