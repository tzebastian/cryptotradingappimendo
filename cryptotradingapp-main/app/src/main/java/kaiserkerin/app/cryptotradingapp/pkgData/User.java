package kaiserkerin.app.cryptotradingapp.pkgData;

import com.google.firebase.firestore.Exclude;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import kaiserkerin.app.cryptotradingapp.pkgAPI.CryptoCurrDataPoint;
import kaiserkerin.app.cryptotradingapp.pkgAPI.CryptoList;
import kaiserkerin.app.cryptotradingapp.pkgFirestore.FirestoreWriteUser;
import kaiserkerin.app.cryptotradingapp.pkgFirestore.OnReadWriteCompleted;

public class User {
    private static User currentUser;

    String username;
    HashMap<String, Double> portfolio;

    @Exclude
    ArrayList<Trade> transactions;

    private User() {
        initObjects();
    }

    private User(String username, HashMap<String, Double> portfolio, ArrayList<Trade> transactions) {
        initObjects();

        this.username = username;
        this.portfolio = portfolio;
        this.transactions = transactions;
    }

    public static void clearInstance() {
        currentUser = null;
    }

    public static User getInstance() {
        if (currentUser == null)
            currentUser = new User();
        return currentUser;
    }

    public static void setInstance(User user) {
        User.currentUser = user;
    }

    public static void saveToDB(OnReadWriteCompleted listener) {
        FirestoreWriteUser fswu = new FirestoreWriteUser(listener);
        fswu.doWriting(currentUser);
    }

    private void initObjects() {
        portfolio = new HashMap<>();
        transactions = new ArrayList<>();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public HashMap<String, Double> getPortfolio() {
        return (HashMap<String, Double>) portfolio.clone();
    }

    public void setPortfolio(HashMap<String, Double> portfolio) {
        this.portfolio = portfolio;
    }

    private void addAmount(String key, Double amount) {
        if (!portfolio.containsKey(key))
            portfolio.put(key, amount);
        else
            portfolio.put(key, portfolio.get(key) + amount);
    }

    private void withdrawAmount(String key, Double amount) throws Exception {
        if (!portfolio.containsKey(key))
            throw new Exception("nothing of " + key + " available!");

        if (portfolio.get(key) < amount)
            throw new Exception("too little of " + key + " available!");

        portfolio.put(key, portfolio.get(key) - amount);

        if(portfolio.get(key) == 0)
            portfolio.remove(key);
    }

    public void buyCrypto(String currToBuySymbol, String rootCurrSymbol, Double amountInRootCurr) throws Exception {
        List<CryptoCurrDataPoint> currencies = CryptoList.getCryptoList();
        Double amountToBuyInEUR = amountInRootCurr;

        CryptoCurrDataPoint currToBuy = currencies.stream()
                .filter(curr -> curr.getSymbol().equals(currToBuySymbol))
                .findFirst().orElse(null);

        if (!rootCurrSymbol.equals(CryptoList.getFiatSymbol())) {
            System.out.println("ROOT: " + rootCurrSymbol);
            CryptoCurrDataPoint rootCurr = currencies.stream()
                    .filter(curr -> curr.getSymbol().equals(rootCurrSymbol))
                    .findFirst().orElse(null);
            amountToBuyInEUR = rootCurr.getQuote().getEUR().getPrice();
        }

        Double amountInTargetCurr = amountToBuyInEUR / currToBuy.getQuote().getEUR().getPrice();
        doTransaction(currToBuySymbol, rootCurrSymbol, amountInTargetCurr, amountInRootCurr, currToBuy.getQuote().getEUR().getPrice());
    }

    public void sellCryptoToFiat(String currToSellSymbol, Double amountInCurrToSell) throws Exception {
        List<CryptoCurrDataPoint> currencies = CryptoList.getCryptoList();

        CryptoCurrDataPoint currToBuy = currencies.stream()
                .filter(curr -> curr.getSymbol().equals(currToSellSymbol))
                .findFirst().orElse(null);
        Double amountBought = amountInCurrToSell * currToBuy.getQuote().getEUR().getPrice();

        doTransaction(CryptoList.getFiatSymbol(), currToSellSymbol, amountBought, amountInCurrToSell, currToBuy.getQuote().getEUR().getPrice());
    }

    public void doTransaction(String targetCurrSymbol, String rootCurrSymbol, Double amountBought, Double amountInRootCurr, Double price) throws Exception {
        Calendar c = Calendar.getInstance();
        withdrawAmount(rootCurrSymbol, amountInRootCurr);
        addAmount(targetCurrSymbol, amountBought);
        transactions.add(new Trade(
                targetCurrSymbol,
                rootCurrSymbol,
                amountBought,
                amountInRootCurr,
                price,
                new Date(
                        c.get(Calendar.YEAR),
                        c.get(Calendar.MONTH) + 1,
                        c.get(Calendar.DAY_OF_MONTH),
                        c.get(Calendar.HOUR_OF_DAY),
                        c.get(Calendar.MINUTE),
                        c.get(Calendar.SECOND)
                )
        ));
    }

    @Exclude
    public ArrayList<Trade> getTransactions() {
        return transactions;
    }

    public void setTransactions(ArrayList<Trade> transactions) {
        this.transactions = transactions;
    }

    public void addFiat(Double amount) {
        this.addAmount(CryptoList.getFiatSymbol(), amount);
    }
}
