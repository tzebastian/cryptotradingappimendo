package kaiserkerin.app.cryptotradingapp.pkgData;

public class Trade {
    String symbol;
    String rootCurrSymbol;
    Double amount, amountInRootCurr, priceBought;
    Date date;

    public Trade() {
    }

    public Trade(String symbol, String rootCurrSymbol, Double amount, Double amountInRootCurr, Double priceBoughtInEUR, Date date) {
        this.symbol = symbol;
        this.rootCurrSymbol = rootCurrSymbol;
        this.amount = amount;
        this.amountInRootCurr = amountInRootCurr;
        this.priceBought = priceBoughtInEUR;
        this.date = date;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getRootCurrSymbol() {
        return rootCurrSymbol;
    }

    public void setRootCurrSymbol(String rootCurrSymbol) {
        this.rootCurrSymbol = rootCurrSymbol;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getAmountInRootCurr() {
        return amountInRootCurr;
    }

    public void setAmountInRootCurr(Double amountInRootCurr) {
        this.amountInRootCurr = amountInRootCurr;
    }

    public Double getPriceBought() {
        return priceBought;
    }

    public void setPriceBought(Double priceBought) {
        this.priceBought = priceBought;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
