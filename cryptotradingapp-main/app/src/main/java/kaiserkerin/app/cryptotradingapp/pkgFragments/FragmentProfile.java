package kaiserkerin.app.cryptotradingapp.pkgFragments;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.util.Collection;

import kaiserkerin.app.cryptotradingapp.R;
import kaiserkerin.app.cryptotradingapp.pkgData.User;
import kaiserkerin.app.cryptotradingapp.pkgFirestore.OnReadWriteCompleted;
import kaiserkerin.app.cryptotradingapp.pkgMisc.ToastManager;

public class FragmentProfile extends Fragment implements View.OnClickListener, OnReadWriteCompleted {
    View view;
    TextView lblUsername;
    Button bttnAddFiat;
    TextView txtAmountFiat;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_profile, container, false);

        getAllViews();
        registerEventhandlers();
        initOtherThings();

        return view;
    }

    private void getAllViews() {
        lblUsername = view.findViewById(R.id.lblUsername);
        txtAmountFiat = view.findViewById(R.id.txtAmountFiat);
        bttnAddFiat = view.findViewById(R.id.bttnAddFiat);
    }

    private void registerEventhandlers() {
        bttnAddFiat.setOnClickListener(this);
    }

    private void initOtherThings() {
        lblUsername.setText(User.getInstance().getUsername());
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.bttnAddFiat:
                String inputAmountFiat = txtAmountFiat.getText().toString();

                if (inputAmountFiat.length() == 0)
                    ToastManager.createErrorToast(getContext(), "Amount cannot be empty!");
                else {
                    Double amountFiat = Double.parseDouble(inputAmountFiat);
                    User.getInstance().addFiat(amountFiat);
                    User.saveToDB(this);
                }

                break;
        }
    }

    @Override
    public void onReadCompleted(Collection<?> collData) {

    }

    @Override
    public void onWriteCompleted(String message) {
        ToastManager.createSuccessToast(getContext(), "Updated portfolio!");
    }

    @Override
    public void onChangeDoc(String message) {

    }

    @Override
    public void onError(String message) {

    }
}
