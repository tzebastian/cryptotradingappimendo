package kaiserkerin.app.cryptotradingapp.pkgActivity;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Collection;

import kaiserkerin.app.cryptotradingapp.R;
import kaiserkerin.app.cryptotradingapp.pkgData.User;
import kaiserkerin.app.cryptotradingapp.pkgFirestore.FirestoreReadUser;
import kaiserkerin.app.cryptotradingapp.pkgFirestore.FirestoreWriteUser;
import kaiserkerin.app.cryptotradingapp.pkgFirestore.OnReadWriteCompleted;
import kaiserkerin.app.cryptotradingapp.pkgMisc.ToastManager;

public class ActivityLogin extends AppCompatActivity implements View.OnClickListener, OnReadWriteCompleted {

    TextView txtUsername;
    Button bttnLogin;
    Button bttnRegister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        System.out.println("test");
        setContentView(R.layout.activity_login);
        try {
            getAllViews();
            registerEventhandlers();
            initOtherThings();
        } catch (Exception ex) {
            Log.e("******* ERROR *******", ex.getMessage());
        }
    }

    ActivityResultLauncher<Intent> registerResultLauncher;

    private void getAllViews() {
        txtUsername = findViewById(R.id.txtUsername);
        bttnLogin = findViewById(R.id.bttnLogin);
        bttnRegister = findViewById(R.id.bttnRegister);
    }

    private void registerEventhandlers() {
        bttnLogin.setOnClickListener(this);
        bttnRegister.setOnClickListener(this);
    }

    private void initOtherThings() {
        registerResultLauncher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                result -> {
                    System.out.println("ON RESULT");
                    if (result.getResultCode() == RESULT_CANCELED)
                        ToastManager.createErrorToast(getApplicationContext(), "cancelled register!");
                    else {
                        String registeredName = result.getData().getData().toString();
                        ToastManager.createSuccessToast(getApplicationContext(), "successfully registered " + registeredName);
                        txtUsername.setText(registeredName);
                    }
                }
        );
    }

    @Override
    public void onClick(View eventView) {
        switch (eventView.getId()) {
            case R.id.bttnLogin:
                String inputUsername = txtUsername.getText().toString();

                if (inputUsername.length() == 0)
                    ToastManager.createErrorToast(getApplicationContext(), "Username cannot be empty!");
                else {
                    FirestoreReadUser fsru = new FirestoreReadUser(this);
                    fsru.doReading(inputUsername);
                }
                break;
            case R.id.bttnRegister:
                openRegisterActivity();
                break;
        }
    }

    private void openMainActivity() {
        Intent i = new Intent(ActivityLogin.this, ActivityMain.class);
        startActivity(i);
    }

    private void openRegisterActivity() {
        Intent intent = new Intent(this, ActivityRegister.class);
        registerResultLauncher.launch(intent);
    }

    @Override
    public void onReadCompleted(Collection<?> collData) {
        if (collData.size() == 0) {
            ToastManager.createErrorToast(getApplicationContext(),"user " + txtUsername.getText().toString() + " is not registered!");
        } else if (collData.size() == 1) {
            User.setInstance((User) collData.stream().iterator().next());
            openMainActivity();
        }
    }

    @Override
    public void onWriteCompleted(String message) {
        openMainActivity();
    }

    @Override
    public void onChangeDoc(String message) {

    }

    @Override
    public void onError(String message) {

    }
}