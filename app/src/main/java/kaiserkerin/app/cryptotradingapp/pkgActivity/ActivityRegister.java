package kaiserkerin.app.cryptotradingapp.pkgActivity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.Collection;

import kaiserkerin.app.cryptotradingapp.R;
import kaiserkerin.app.cryptotradingapp.pkgData.User;
import kaiserkerin.app.cryptotradingapp.pkgFirestore.FirestoreWriteUser;
import kaiserkerin.app.cryptotradingapp.pkgFirestore.OnReadWriteCompleted;
import kaiserkerin.app.cryptotradingapp.pkgMisc.ToastManager;

public class ActivityRegister extends AppCompatActivity implements View.OnClickListener, OnReadWriteCompleted {

    TextView txtUsername;
    Button bttnRegister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        try {
            getAllViews();
            registerEventhandlers();
        } catch (Exception ex) {
            Log.e("******* ERROR *******", ex.getMessage());
        }
    }

    String usernameToRegister;

    private void getAllViews() {
        bttnRegister = findViewById(R.id.bttnRegister);
        txtUsername = findViewById(R.id.txtUsername);
    }

    private void registerEventhandlers() {
        bttnRegister.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.bttnRegister:
                usernameToRegister = txtUsername.getText().toString();

                if (usernameToRegister.length() == 0)
                    ToastManager.createErrorToast(getApplicationContext(),"Username cannot be empty!");
                else {
                    User.clearInstance();
                    User user = User.getInstance();
                    user.setUsername(txtUsername.getText().toString());

                    FirestoreWriteUser fswu = new FirestoreWriteUser(this);
                    fswu.doWriting(user);
                }
                break;
        }
    }

    @Override
    public void onReadCompleted(Collection<?> collData) {

    }

    @Override
    public void onWriteCompleted(String message) {
        Intent data = new Intent();
        data.setData(Uri.parse(usernameToRegister));
        setResult(RESULT_OK, data);
        finish();
    }

    @Override
    public void onChangeDoc(String message) {

    }

    @Override
    public void onError(String message) {

    }
}
