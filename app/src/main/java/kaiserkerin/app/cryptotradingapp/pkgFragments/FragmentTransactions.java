package kaiserkerin.app.cryptotradingapp.pkgFragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.util.Collection;

import kaiserkerin.app.cryptotradingapp.R;
import kaiserkerin.app.cryptotradingapp.pkgData.Trade;
import kaiserkerin.app.cryptotradingapp.pkgData.User;
import kaiserkerin.app.cryptotradingapp.pkgFirestore.FirestoreReadTransactions;
import kaiserkerin.app.cryptotradingapp.pkgFirestore.OnReadWriteCompleted;
import kaiserkerin.app.cryptotradingapp.pkgMisc.CustomizedAdapterTrade;

public class FragmentTransactions extends Fragment implements OnReadWriteCompleted {

    View view;
    ListView listTransactions;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_transactions, container, false);

        getAllViews();
        initOtherThings();

        return view;
    }

    User currentUser;
    FirestoreReadTransactions frTransactions;

    private void getAllViews() {
        listTransactions = view.findViewById(R.id.listTransactions);
    }

    private void initOtherThings() {
        currentUser = User.getInstance();

        frTransactions = new FirestoreReadTransactions(this);
        frTransactions.doReading(currentUser.getUsername());
    }

    private void fillListViewTransactions(Collection<Trade> collTransactions) {
        CustomizedAdapterTrade adapter = new CustomizedAdapterTrade(getContext(), collTransactions);
        listTransactions.setAdapter(adapter);
    }

    @Override
    public void onReadCompleted(Collection<?> collData) {
        fillListViewTransactions((Collection<Trade>) collData);
    }

    @Override
    public void onWriteCompleted(String message) {

    }

    @Override
    public void onChangeDoc(String message) {

    }

    @Override
    public void onError(String message) {

    }
}
