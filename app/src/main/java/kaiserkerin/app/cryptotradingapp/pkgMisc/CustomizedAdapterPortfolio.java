package kaiserkerin.app.cryptotradingapp.pkgMisc;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import kaiserkerin.app.cryptotradingapp.R;
import kaiserkerin.app.cryptotradingapp.pkgAPI.CryptoList;

public class CustomizedAdapterPortfolio extends BaseAdapter {

    ArrayList<String> collPortfolioKeys = new ArrayList<>();
    HashMap<String, Double> mapPortfolio = new HashMap<>();
    LayoutInflater inflater;

    public CustomizedAdapterPortfolio(Context applicationContext, Map<String, Double> collPortfolio) {
        this.mapPortfolio.putAll(collPortfolio);
        this.collPortfolioKeys = new ArrayList<>(collPortfolio.keySet());
        this.inflater = LayoutInflater.from(applicationContext);
    }

    @Override
    public int getCount() {
        return mapPortfolio.size();
    }

    @Override
    public Object getItem(int i) {
        return collPortfolioKeys.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        try {
            view = inflater.inflate(R.layout.portfolio_listentry, null);
            TextView assetName = view.findViewById(R.id.lblAssetName);
            TextView assetAmount = view.findViewById(R.id.lblAssetAmount);
            TextView assetAmountEUR = view.findViewById(R.id.lblAssetAmountInEUR);
            String currAssetSymbol = (String) getItem(i);

            Double amount = mapPortfolio.get(currAssetSymbol);
            Double amountInEUR = amount;

            if (!currAssetSymbol.equals(CryptoList.getFiatSymbol())) {
                amountInEUR = CryptoList.getCryptoList().stream()
                        .filter(curr -> curr.getSymbol().equals(currAssetSymbol))
                        .findFirst().orElse(null).getQuote().getEUR().getPrice() * amount;
            }

            DecimalFormat df = new DecimalFormat();
            df.setMaximumFractionDigits(6);

            assetName.setText(currAssetSymbol);
            assetAmount.setText(df.format(amount) + " " + currAssetSymbol);
            assetAmountEUR.setText(df.format(amountInEUR) + " EUR");

            return view;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }
}
