package kaiserkerin.app.cryptotradingapp.pkgMisc;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import kaiserkerin.app.cryptotradingapp.R;
import kaiserkerin.app.cryptotradingapp.pkgAPI.CryptoCurrDataPoint;
import kaiserkerin.app.cryptotradingapp.pkgData.User;
import kaiserkerin.app.cryptotradingapp.pkgFirestore.OnReadWriteCompleted;

public class SellDialog extends BottomSheetDialog implements View.OnClickListener, OnReadWriteCompleted {
    public static CryptoCurrDataPoint selectedCrypto;

    public Activity c;
    public Dialog d;

    TextView lblCryptoSymbol;
    EditText txtAmount;
    Button bttnSell;
    Button bttnMax;

    public SellDialog(Activity a) {
        super(a);
        this.c = a;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.sell_dialog);

        getViews();
        initOtherThings();
        registerEventhandler();
    }

    private void getViews() {
        lblCryptoSymbol = findViewById(R.id.lblCryptoSymbol);
        txtAmount = findViewById(R.id.txtAmount);
        bttnSell = findViewById(R.id.bttnSell);
        bttnMax = findViewById(R.id.bttnMax);
    }

    private void initOtherThings() {
        List<String> availableCurrencies = new ArrayList<>(User.getInstance().getPortfolio().keySet());
        System.out.println(User.getInstance().getUsername() + " " + availableCurrencies);
        lblCryptoSymbol.setText("SELL " + selectedCrypto.getSymbol());
        txtAmount.setHint("amount to sell of " + selectedCrypto.getSymbol());
    }

    private void registerEventhandler() {
        bttnSell.setOnClickListener(this);
        bttnMax.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        try {
            switch (v.getId()) {
                case R.id.bttnSell:
                    if (txtAmount.getText().length() == 0)
                        throw new Exception("Amount cannot be empty!");

                    User.getInstance().sellCryptoToFiat(selectedCrypto.getSymbol(), Double.parseDouble(txtAmount.getText().toString()));
                    User.saveToDB(this);
                    dismiss();
                    break;
                case R.id.bttnMax:
                    Double amount = User.getInstance().getPortfolio().get(selectedCrypto.getSymbol());

                    if(amount == null)
                        throw new Exception("Nothing to sell!");

                    txtAmount.setText(amount.toString());
                    break;
                default:
                    break;
            }
        } catch (Exception ex) {
            ToastManager.createErrorToast(getContext(), ex.getMessage());
            ex.printStackTrace();
        }
    }

    @Override
    public void onReadCompleted(Collection<?> collData) {

    }

    @Override
    public void onWriteCompleted(String message) {
        ToastManager.createSuccessToast(getContext(), "completed!");
    }

    @Override
    public void onChangeDoc(String message) {

    }

    @Override
    public void onError(String message) {

    }
}