package kaiserkerin.app.cryptotradingapp.pkgFirestore;

import java.util.Collection;

public interface OnReadWriteCompleted {
    void onReadCompleted(Collection<?> collData);
    void onWriteCompleted(String message);
    void onChangeDoc(String message);
    void onError(String message);
}