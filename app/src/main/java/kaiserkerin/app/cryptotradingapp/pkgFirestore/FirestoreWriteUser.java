package kaiserkerin.app.cryptotradingapp.pkgFirestore;

import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;

import kaiserkerin.app.cryptotradingapp.pkgData.Trade;
import kaiserkerin.app.cryptotradingapp.pkgData.User;

public class FirestoreWriteUser implements OnFailureListener, OnCompleteListener<Void>, OnSuccessListener<Void> {
    private static String COLL_USERS = "collUsers";
    private static String COLL_TRANSACTIONS = "collTransactions";
    private FirebaseFirestore ffdb;
    private OnReadWriteCompleted listener;

    public FirestoreWriteUser(OnReadWriteCompleted listener) {
        this.listener = listener;
        this.ffdb = FirebaseFirestore.getInstance();
    }

    public void doWriting(User paraUser) {
        ffdb.collection(COLL_USERS)
                .document(String.valueOf(paraUser.getUsername()))
                .set(paraUser)
                .addOnSuccessListener(animalCreateTask -> {
                    setTransactionsOfUser(paraUser);
                })
                .addOnFailureListener(this)
                .addOnCompleteListener(this);
    }

    public void setTransactionsOfUser(User user) {
        for (Trade t : user.getTransactions()){
            ffdb.collection(COLL_USERS).document(user.getUsername())
                    .collection(COLL_TRANSACTIONS)
                    .document(t.getDate().toString())
                    .set(t)
                    .addOnSuccessListener(this);
        }
    }

    @Override
    public void onComplete(@NonNull Task<Void> task) {
        Log.i("oncomplete", "ok");
        listener.onWriteCompleted("write ok!");
    }

    @Override
    public void onFailure(@NonNull Exception e) {
        Log.e("not succ", e.getMessage());
        listener.onWriteCompleted("write problem : " + e.getMessage());
    }

    @Override
    public void onSuccess(Void unused) {
        Log.i("onsuccess", "success");
    }
}
