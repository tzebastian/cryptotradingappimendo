package kaiserkerin.app.cryptotradingapp.pkgAPI;

import java.io.Serializable;
import java.util.List;

import com.google.android.gms.common.api.Status;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CryptoList implements Serializable {
    private static String FiatSymbol = "EUR";
    private static CryptoList instance = null;
    @SerializedName("status")
    @Expose
    private Status status;
    @SerializedName("data")
    @Expose
    private List<CryptoCurrDataPoint> data = null;
    private final static long serialVersionUID = -4369048252305703014L;

    public static void setInstance(CryptoList cryptoList) {
        instance = cryptoList;
    }

    public static List<CryptoCurrDataPoint> getCryptoList() throws Exception {
        if(instance == null)
            throw new Exception("Cryptolist has not been loaded!");
        return instance.getData();
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public List<CryptoCurrDataPoint> getData() {
        return data;
    }

    public void setData(List<CryptoCurrDataPoint> data) {
        this.data = data;
    }

    public static String getFiatSymbol() {
        return FiatSymbol;
    }

    public static double getCurrentFiatValue(String symbol, double amount) {
        if(symbol.equals(CryptoList.getFiatSymbol()))
            return amount;

        CryptoCurrDataPoint currToBuy = instance.getData().stream()
                .filter(curr -> curr.getSymbol().equals(symbol))
                .findFirst().orElse(null);
        return amount * currToBuy.getQuote().getEUR().getPrice();
    }

}
